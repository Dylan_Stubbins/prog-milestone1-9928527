﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Would you like to convert celcius or fahrenheit?");

            var metric = Console.ReadLine();
            switch (metric)
            {
                case "celcius":
                    Console.WriteLine("How many degrees celcius would you like converted?");
                    int celcius = int.Parse(Console.ReadLine());
                    Console.WriteLine();

                    int CtoF = ((celcius * 9) / 5) + 32;
                    Console.WriteLine($"This is {CtoF} degrees fahrenheit");
                    Console.ReadLine();
                    break;

                case "fahrenheit":
                    Console.WriteLine("How many degrees fahrenheit would you like converted?");
                    int fahrenheit = int.Parse(Console.ReadLine());
                    Console.WriteLine();

                    int FtoC = ((fahrenheit - 32) / 9) * 5;
                    Console.WriteLine($"This is {FtoC} degrees celcius");
                    Console.ReadLine();
                    break;

                default:
                    Console.WriteLine("Please type celcius or fahrenheit");
                    break;
            }
         }
    }
}
