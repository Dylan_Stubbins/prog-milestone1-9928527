﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            Console.Write("Enter a number ");
            i = int.Parse(Console.ReadLine());
            if (i % 2 == 0)
            {
                Console.Write("This is an even number");
                Console.Read();
            }
            else
            {
                Console.Write("This is an odd number");
                Console.Read();
            }
        }
    }
}
