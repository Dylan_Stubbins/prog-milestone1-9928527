﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var calender = new Dictionary<String, int>();

            calender.Add("January", 31);
            calender.Add("February", 29);
            calender.Add("March", 31);
            calender.Add("April", 30);
            calender.Add("May", 31);
            calender.Add("June", 30);
            calender.Add("July", 31);
            calender.Add("August", 31);
            calender.Add("September", 30);
            calender.Add("October", 31);
            calender.Add("November", 30);
            calender.Add("December", 31);

            Console.WriteLine($"{31}"); //Print all months with 31 days
        }
    }
}
