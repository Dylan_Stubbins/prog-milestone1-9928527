﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            var year = 0;
            Console.WriteLine("Which year would you like to check is a leap year?");
            year = int.Parse(Console.ReadLine());

            if (year % 4 == 0)
                {
                Console.WriteLine("Yes, this is a leap year");
            }
            else
            {
                Console.WriteLine("No, this is not a leap year");
            }
        }
    }
}
