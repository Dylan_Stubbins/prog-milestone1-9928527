﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32
{
    class Program
    {
        static void Main(string[] args)
        {
            var weekDays = new string[7] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
            Console.WriteLine("Please type in any day of the week");
            var output = Console.ReadLine();

            weekDays[0] = "1st day of the week";
            weekDays[1] = "2nd day of the week";
            weekDays[2] = "3rd day of the week";
            weekDays[3] = "4th day of the week";
            weekDays[4] = "5th day of the week";
            weekDays[5] = "6th day of the week";
            weekDays[6] = "7th day of the week";
            {
            Console.WriteLine($"{output}");
            }
        }
    }
}
