﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Would you like to convert miles or kilometers?");

            var metric = Console.ReadLine();
            const double mile2km = 1.609344;
            const double km2mile = 0.621371;
            switch (metric)
            {
                case "miles":
                    Console.WriteLine("How many miles would you like converted?");
                    var miles = double.Parse(Console.ReadLine());
                    var getKilometers = (miles * mile2km);
                    Console.WriteLine($"{miles} miles is {getKilometers} kilometers");
                    break;

                case "kilometers":
                    Console.WriteLine("How many kilometers would you like converted?");
                    var kilometers = double.Parse(Console.ReadLine());
                    var getMiles = (kilometers * km2mile);
                    Console.WriteLine($"{kilometers} kilometers is {getMiles} miles");
                    break;

                default:
                    Console.WriteLine("Please type kilometers or miles");
                    break;
            }




        }
    }
}
