﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = 5;
            do
            {
                Console.Clear();

                Console.WriteLine("!!MAIN MENU!!");
                Console.WriteLine("             ");
                Console.WriteLine($"1. Option 01");
                Console.WriteLine($"2. Option 02");
                Console.WriteLine($"3. Option 03");
                Console.WriteLine($"4. Option 04");
                Console.WriteLine("**********");
                Console.WriteLine($"Please select an option");
                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();
                    Console.WriteLine($"You chose Option 01");
                    Console.WriteLine($"Enter 5 to go back to the main menu.");
                    menu = int.Parse(Console.ReadLine());
                }
                if (menu == 2)
                {
                    Console.Clear();
                    Console.WriteLine($"You chose Option 02");
                    Console.WriteLine($"Enter 5 to go back to the main menu.");
                    menu = int.Parse(Console.ReadLine());
                }
                if (menu == 3)
                {
                    Console.Clear();
                    Console.WriteLine($"You chose Option 03");
                    Console.WriteLine($"Enter 5 to go back to the main menu.");
                    menu = int.Parse(Console.ReadLine());
                }
                if (menu == 4)
                {
                    Console.Clear();
                    Console.WriteLine($"You chose Option 04");
                    Console.WriteLine($"Enter 5 to go back to the main menu.");
                    menu = int.Parse(Console.ReadLine());
                }
                

            } while (menu == 5);

            if (menu > 5)
            {
                Console.WriteLine($"Please choose the correct option");
                Console.WriteLine($"Press <ENTER> to return to the main menu");
                Console.ReadLine();
                Console.Clear();
            }
        }
     }
}
        
    

