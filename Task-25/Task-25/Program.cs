﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 0;
            Console.WriteLine("Please enter in any number");
            var input = Console.ReadLine();
            bool value = int.TryParse(input, out a);

            Console.WriteLine($"You entered in {input}");
        }
    }
}
