﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, string, int>>();

            names.Add(Tuple.Create("Daniel", "March", 29));
            names.Add(Tuple.Create("Daryl", "August", 23));
            names.Add(Tuple.Create("Dylan", "April", 22));

            foreach (var item in names)
            {
                Console.WriteLine($"{item.Item1} {item.Item2} {item.Item3}");
            }
        }
    }
}
