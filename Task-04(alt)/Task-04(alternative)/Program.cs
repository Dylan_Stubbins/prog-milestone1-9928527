﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_alternative_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Would you like to convert celcius or fahrenheit?");

            var metric = Console.ReadLine();
            const double Fh2C = 0.555556;//need to work out the correct calculation//
            const double C2Fh = 32;
            switch (metric)
            {
                case "celcius":
                    Console.WriteLine("How much degrees celcius would you like converted?");
                    var celcius = double.Parse(Console.ReadLine());
                    var getFahrenheit = (celcius * C2Fh);
                    Console.WriteLine($"{celcius} degrees celcius is {getFahrenheit} degrees fahrenheit");
                    break;

                case "fahrenheit":
                    Console.WriteLine("How much degrees fahrenheit would you like converted?");
                    var fahrenheit = double.Parse(Console.ReadLine());
                    var getCelcius = (fahrenheit * Fh2C);
                    Console.WriteLine($"{fahrenheit} degrees fahrenheit is {getCelcius} degrees celcius");
                    break;

                default:
                    Console.WriteLine("Please type celcius or fahrenheit");
                    break;
            }
    }
}
