﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_34
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type in the amount of weeks you would like to know how many working days are in them");
            const int workDays2Week = 5;

            var Week = int.Parse(Console.ReadLine());
            Console.Clear();
            var getworkDays = (Week * workDays2Week);
            Console.WriteLine($"{Week} weeks is {getworkDays} work days");

        }
    }
}
