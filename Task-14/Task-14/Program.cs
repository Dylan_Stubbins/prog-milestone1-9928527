﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            const int TB2GB = 1024;
            const int GB2TB = 1;
            Console.WriteLine("Please enter the amount of TBs you want converted into GBs");
            var terabyte = int.Parse(Console.ReadLine());
            var getGB = (terabyte * TB2GB);

            Console.WriteLine($"{terabyte} Terabytes is {getGB} Gigabytes");
            
        }
    }
}
